# Handball data

This repo contains a dataset (Games_Men.csv) with results of national teams games, ranging from 1992 to 2023. For now, only male national teams results are in the database.

The rationale for this database is to construct a rating system. (Ongoing work...)

After the 2023 World Championship (congratulations to the danish team !), the database contains 6,301 game results.

The csv file has 7 columns :

* Event : event (Olympic Games, World Championship, European Championship, etc.). 
* EventYear : Year of the main event; for qualification tournament, since some may span over several year (such as the qualification tournaments for the European Championship), the year is the year of tournament for which the teams are competing.
* Date : date of the game, format YYYY-MM-DD
* Team1 : name of the 1rst team (IOC country codes)
* Team2 : name of the 2nd team (IOC country codes)
* Score1 : score of the 1rst team 
* Score2 : score of the 2nd team

[IOC country codes](https://en.wikipedia.org/wiki/List_of_IOC_country_codes)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.


If you find any errors, or if you have any suggestions, or anything, feel free to [send me an email](mailto:franck.arnaud@ensae.org) (no spam thx).



# Championships !

The major events are :

* The Olympic games (Barcelona 1992, Atlanta 1996, Sidney 2000, Athens 2004, Beijing 2008, London 2012, Rio 2016, Tokyo 2021) every four years except for Japan 2020 that was rescheduled in 2021, due to Covid;
* The World Championship, every two years. Data ranges from the 1993 WC to the 2021 WC.
* The continents championships :

	+ the European Championship (1994 to 2022)
	+ the Asian Championship (1995, 2000, 2004, 2006, 2008, 2010, 2012, 2014 2016, 2018, 2020, 2022)
	+ the African Championship (every two years, from 1998 to 2020)
	+ the American Championship
	+ the Oceanian Championship
	
* Some other championships : Panamerica games, Mediterranean games, IHF Emerging Nations Competition, Golden League, etc.


## 2022 Asian Men's Handball Championship
[Wikipedia page](https://en.wikipedia.org/wiki/2022_Asian_Men's_Handball_Championship)


## 2023 World Championship
[Wikipedia page](https://en.wikipedia.org/wiki/2023_World_Men%27s_Handball_Championship)
